import React from 'react';
import ListaLembretes from './Views/ListaLembretes';
import CadastroLembrete from './Views/CadastroLembrete';
import EditarLembrete from './Views/EditarLembrete';
import { View } from 'react-native'
import {NativeRouter, Switch, Route} from 'react-router-native'

export default class App extends React.Component {
  render () {
    return (
      <NativeRouter>
        <View style={{ flex: 1 }}>
          <Switch>
            <Route path='/cadastro' component={CadastroLembrete} />
            <Route path='/:pageId' component={EditarLembrete} />
            <Route component={ListaLembretes} />
          </Switch>
        </View>
      </NativeRouter>
    )
  }
}